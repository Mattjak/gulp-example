const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const browserSync = require('browser-sync'); // Подключаем Browser Sync
const htmlmin = require('gulp-html-minifier2');
const imagemin = require('gulp-imagemin');
const clean = require('gulp-clean');
const cleanCSS = require('gulp-clean-css');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const terser = require('gulp-terser');

const cleanTask = () => {
    return gulp.src('./build', {read: false})
        .pipe(clean());
};

const html = () => {
    return gulp.src('./src/**.html')
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('./build'))
};


const scss = () => {
    return gulp.src('src/scss/**')
        .pipe(sourcemaps.init())
        .pipe(sass())// Преобразуем Sass в CSS посредством gulp-sass
        .pipe(autoprefixer())
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('build/css')) // Выгружаем результата в папку build/css
};

const js = () => {
    return gulp.src('./src/js/**/*.js')
        .pipe(sourcemaps.init())
        .pipe(concat('bundle.js'))
        .pipe(terser())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./build/js'));
};

const image = () => {
    return gulp.src('./src/img/**/*.*')
        .pipe(imagemin())
        .pipe(gulp.dest('./build/img'))
};

const watcher = () => {
    browserSync({ // Выполняем browser Sync
        server: { // Определяем параметры сервера
            baseDir: './build' // Директория для сервера - build
        },
        notify: false // Отключаем уведомления
    });
    gulp.watch("./src/scss/**/*.scss", scss).on("change", browserSync.reload);
    gulp.watch("./src/index.html", html).on("change", browserSync.reload);
    gulp.watch("./src/js/**/*.js", js).on("change", browserSync.reload);
};


gulp.task("default", gulp.series(
    cleanTask,
    html,
    scss,
    js,
    image,
    watcher
));