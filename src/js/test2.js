$(document).ready(function () {
    $('.next').click(function () {
        let currentImage = $(".img.current");
        let curImageIndex = currentImage.index();
        let nextImageIndex  = curImageIndex + 1;
        let nextImage = $('.img').eq(nextImageIndex);
        currentImage.fadeOut(0);
        currentImage.removeClass("current");
        if (nextImageIndex === ($('.img:last').index()+1)) {
            $('.img').eq(0).fadeIn(0);
            $('.img').eq(0).addClass('current');
        }
        else{
            nextImage.fadeIn(0);
            nextImage.addClass('current');
        }
    });
    $('.prev').click(function () {
        let currentImage = $(".img.current");
        let curImageIndex = currentImage.index();
        let prevImageIndex  = curImageIndex - 1;
        let prevImage = $('.img').eq(prevImageIndex);
        currentImage.fadeOut(0);
        currentImage.removeClass("current");
        prevImage.fadeIn(0);
        prevImage.addClass("current")

    })
});